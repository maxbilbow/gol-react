import React, { Component } from 'react';
import './App.css';
import GameOfLife from "./game/GameOfLife";

class App extends Component {
  render() {
    return (
      <div className="App">
        <GameOfLife rows={100} cols={160}/>
      </div>
    );
  }
}

export default App;
