import React, {Component} from "react";
import GolCell from "./GolCell";

import _ from "lodash";

const TICK_RATE = 100;

class GameOfLife extends Component {
    state = {
        board: []
    };
    cells = [];
    constructor(props) {
        super(props);
        for (let i = 0; i < props.rows; ++i) {
            const cols = [];
            for (let j = 0; j < props.cols; ++j) {
                const cell = new GolCell({x: i, y: j, board: this.state.board});
                cols.push(cell);
                this.cells.push(cell);
            }
            this.state.board.push(cols);
        }
        this.tick();
    }

    tick() {
        const start = Date.now();
        _.forEach(this.cells, cell => cell.setNextState());
        _.forEach(this.cells, cell => cell.flip());
        this.setState({});
        const duration = (Date.now() - start);
        if (duration > TICK_RATE) {
            this.tick();
        } else {
            setTimeout(() => this.tick(), duration - TICK_RATE);
        }
    }

    flipCell() {
        this.alive = !this.alive;
        _.forEach(this.neighbours, each => {
            if (Math.random() > 0.5) {
                each.alive = !each.alive;
            }
        });
    }

    renderRow(row) {

        return row.map(cell => (
            <div className={`golBoard__cell golBoard__cell--${cell.alive ? "alive" : "dead"}`} onClick={this.flipCell.bind(cell)}></div>
        ));
    }

    render() {
        return this.state.board.map(row => (
            <div className="golBoard__row">{this.renderRow(row)}</div>
        ));
    }

    static get defaultProps () {
        return {
            rows: 10,
            cols: 10
        }
    }
}

export default GameOfLife;