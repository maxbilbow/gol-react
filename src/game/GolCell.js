import _ from "lodash"

function getNeighbours({board, x, y}) {
    return [
        _.get(board, [x-1, y-1]),
        _.get(board, [x, y-1]),
        _.get(board, [x+1, y-1]),
        _.get(board, [x+1, y]),
        _.get(board, [x+1, y+1]),
        _.get(board, [x, y+1]),
        _.get(board, [x-1, y+1]),
        _.get(board, [x-1, y])
    ].filter(each => each)
}

class GolCell {

    constructor(props) {
        this.props = props;
        this.nextState = this.alive = Math.random() > 0.3;
    }

    get neighbours() {
        if (!this._neighbours) {
            this._neighbours = getNeighbours(this.props);
        }
        return this._neighbours;
    }
    setNextState() {
        const living = this.neighbours.filter(each => each.alive).length;
        if (this.alive) {
           this.nextState = living === 2 || living === 3;
        }
        else {
            this.nextState = living === 3;
        }
    }

    flip() {
        this.alive = this.nextState;
    }
}

export default GolCell;